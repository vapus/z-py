<?php require_once 'engine/init.php'; include 'layout/overall/header.php'; 
if ($config['log_ip']) {
	znote_visitor_insert_detailed_data(4);
}
if (isset($_GET['name']) === true && empty($_GET['name']) === false) {
	$name = $_GET['name'];
	
	if (user_character_exist($name)) {
		$user_id = user_character_id($name);
		$profile_data = user_character_data($user_id, 'name', 'experience', 'vocation', 'lastlogin', 'online');
		$profile_znote_data = user_znote_character_data($user_id, 'hide_char', 'comment');
		$profile_acc_data = user_znote_account_data($user_id, 'created');
		
		$guild_exist = false;
		if (get_character_guild_rank($user_id) > 0) {
			$guild_exist = true;
			$guild = get_player_guild_data($user_id);
			$guild_name = get_guild_name($guild['guild_id']);
		}
		?>
		
		<!-- PROFILE MARKUP HERE-->
			<?php ?>
			<h1><font class="profile_font" name="profile_font_header">Profile: <?php echo $profile_data['name']; ?></font></h1>
			<ul>
				<li><font class="profile_font" name="profile_font_level">Level: <?php echo experience_to_level($profile_data['experience']); ?></font></li>
				<li><font class="profile_font" name="profile_font_vocation">Vocation: <?php echo $config['vocations'][$profile_data['vocation']]; ?></font></li>
				<?php 
				if ($guild_exist) {
				?>
				<li><font class="profile_font" name="profile_font_vocation"><b><?php echo $guild['rank_name']; ?></b> of <a href="guilds.php?name=<?php echo $guild_name; ?>"><?php echo $guild_name; ?></a></font></li>
				<?php
				}
				?>
				<li><font class="profile_font" name="profile_font_lastlogin">Last Login: <?php
					if ($profile_data['lastlogin'] != 0) {
						echo(date($config['date'],$profile_data['lastlogin']));
					} else {
						echo 'Never.';
					}
					
				?></font></li>
				<li><font class="profile_font" name="profile_font_status">Status:</font> <?php 
						if ($profile_data['online'] == 1) {
							echo '<font class="profile_font" name="profile_font_online" color="green"><b>ONLINE</b></font>';
						} else {
							echo '<font class="profile_font" name="profile_font_online" color="red"><b>OFFLINE</b></font>';
						}
					?></li>
				<li><font class="profile_font" name="profile_font_created">Account Created: <?php echo(date($config['date'], $profile_acc_data['created'])); ?></font></li>
				<li><font class="profile_font" name="profile_font_comment">Comment:</font> <br><textarea name="profile_comment_textarea" cols="70" rows="10" readonly="readonly"><?php echo $profile_znote_data['comment']; ?></textarea></li>
				<!-- DEATH LIST -->
				<li>
					<b>Death List:</b><br>
					<?php
					if ($config['TFSVersion'] == 'TFS_02') {
						$array = user_fetch_deathlist($user_id);
						if ($array) {
							//print_r($array);
							?>
							<ul>
								<?php
								// Design and present the list
								foreach ($array as $value) {
									echo '<li>';
									// $value[0]
									$value[1] = date($config['date'],$value[1]);								
									if ($value[4] == 1) {
										$value[3] = 'player: <a href="characterprofile.php?name='. $value[3] .'">'. $value[3] .'</a>';
									} else {
										$value[3] = 'monster: '. $value[3] .'.';
									}
									
									echo '['. $value[1] .'] Killed at level '. $value[2] .' by '. $value[3];
									echo '</li>';
								}
							?>
							</ul>
							<?php
							} else {
								echo '<b><font color="green">This player has never died.</font></b>';
							}
							//Done.
						}
					if ($config['TFSVersion'] == 'TFS_03') {
						$array = user_fetch_deathlist03($user_id);
						if ($array) {
						?>
						<ul>
							<?php
							// Design and present the list
							foreach ($array as $value) {
								echo '<li>';
								$value[3] = user_get_killer_id(user_get_kid($value['id']));
								if ($value[3] !== false && $value[3] >= 1) {
									$namedata = user_character_data((int)$value[3], 'name');
									$value[3] = $namedata['name'];
									$value[3] = 'player: <a href="characterprofile.php?name='. $value[3] .'">'. $value[3] .'</a>';
								} else {
									$value[3] = user_get_killer_m_name(user_get_kid($value['id']));
								}
								echo '['. date($config['date'],$value['time']) .'] Killed at level '. $value['level'] .' by '. $value[3];
								echo '</li>';
							}
						?>
						</ul>
						<?php
						} else {
							echo '<b><font color="green">This player has never died.</font></b>';
						}
					}
						?>
				</li>
				
				<!-- END DEATH LIST -->
				
				<!-- CHARACTER LIST -->
				<?php
				if (user_character_hide($profile_data['name']) != 1 && user_character_list_count(user_character_account_id($name)) > 1) {
				?>
					<li>
						<b>Other visible characters on this account:</b><br>
						<?php
						$array = user_character_list(user_character_account_id($profile_data['name']));
						// Array: [0] = name, [1] = level, [2] = vocation, [3] = town_id, [4] = lastlogin, [5] = online
						if ($array && count($array) > 1) {
							?>
							<table>
								<tr class="yellow">
									<td>
										Name:
									</td>
									<td>
										Level:
									</td>
									<td>
										Vocation:
									</td>
									<td>
										Last login:
									</td>
									<td>
										Status:
									</td>
								</tr>
								<?php
								// Design and present the list
								foreach ($array as $value) {
									if ($value[0] != $profile_data['name']) {
										if (hide_char_to_name(user_character_hide($value['name'])) != 'hidden') {
											echo '<tr>';
											echo '<td><a href="characterprofile.php?name='. urlencode($value['name']) .'">'. $value['name'] .'</a></td>';
											echo '<td>'. $value['level'] .'</td>';
											echo '<td>'. $value['vocation'] .'</td>';
											echo '<td>'. $value['lastlogin'] .'</td>';
											echo '<td>'. $value['online'] .'</td>';
											echo '</tr>';
										}
									}
								}
							?>
							</table>
							<?php
							} else {
								echo '<b><font color="green">This player has never died.</font></b>';
							}
								//Done.
							?>
					</li>
				<?php
				}
				?>
				<!-- END CHARACTER LIST -->
				<li><font class="profile_font" name="profile_font_share_url">Address: <a href="<?php 
					if ($config['htwrite']) echo "http://".$_SERVER['HTTP_HOST']."/". $profile_data['name'];
					else echo "http://".$_SERVER['HTTP_HOST']."/characterprofile.php?name=". $profile_data['name'];
					
				?>"><?php
					if ($config['htwrite']) echo "http://".$_SERVER['HTTP_HOST']."/". $profile_data['name'];
					else echo "http://".$_SERVER['HTTP_HOST']."/characterprofile.php?name=". $profile_data['name'];
				?></a></font></li>
			</ul>
		<!-- END PROFILE MARKUP HERE-->
		
		<?php
	} else {
		echo htmlentities(strip_tags($name, ENT_QUOTES)).' does not exist.';
	}
} else {
	header('Location: index.php');
}

include 'layout/overall/footer.php'; ?>