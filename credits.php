<?php require_once 'engine/init.php'; include 'layout/overall/header.php'; ?>

<h1>Z-PY AAC</h1>
<p>This website is powered by the Znote AAC engine.</p>

<h2>Developers:</h2>
<p>Main developer: <a href="http://vapus.net/forum/members/brannfjell/">Brannfjell</a>.</p>

<h3>Thanks to: (in no particular order)</h3>
<p>
<a href="http://vapus.net/forum/members/stian/">Stian</a> - Repository for this AAC.
<br/><a href="http://vapus.net/forum/members/stian/">Soul4Soul</a> - Making Znote AAC compatible with pyOT.
</p>

<?php include 'layout/overall/footer.php'; ?>