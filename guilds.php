<?php require_once 'engine/init.php';
if ($config['require_login']['guilds']) protect_page();
include 'layout/overall/header.php';

if (user_logged_in() === true) {
	
	// fetch data
	$char_count = user_character_list_count($session_user_id);
	$char_array = user_character_list($user_data['id']);
	
	$characters = array();
	foreach ($char_array as $value) {
		$characters[] = $value['name'];
	}
}

if (empty($_GET['name'])) {
// Display the guild list
?>

<h1>Guild List:</h1>
<?php
$guilds = get_guilds_list();
if ($guilds !== false) {
?>
<table>
	<tr class="yellow">
		<td>Guild name:</td>
		<td>Members:</td>
		<td>Founded:</td>
	</tr>
		<?php
		foreach ($guilds as $guild) {
			$url = url("guilds.php?name=". $guild['name']);
			echo '<tr class="special" onclick="javascript:window.location.href=\'' . $url . '\'">';
			echo '<td>'. $guild['name'] .'</td>';
			echo '<td>'. count_guild_members($guild['guild_id']) .'</td>';
			echo '<td>'. date($config['date'],$guild['created']) .'</td>';
			echo '</tr>';
		}
		?>
</table>
<?php } else echo '<p>Guild list is empty.</p>';?>
<!-- user stuff -->
<?php
if (user_logged_in() === true) {	
	// post verifications
	// CREATE GUILD
	if (!empty($_POST['selected_char']) && !empty($_POST['guild_name'])) {
		if (user_character_account_id($_POST['selected_char']) === $session_user_id) {
			//code here
			$name = $_POST['selected_char'];
			$user_id = user_character_id($name);
			$char_data = user_character_data($user_id, 'experience', 'online');
			
			// If character level is high enough
			if (experience_to_level($char_data['experience']) >= $config['create_guild_level']) {
			
				// If character is offline
				if ($char_data['online'] == 0) {
					$acc_data = user_data($user_data['id'], 'premdays');
					
					// If character is premium
					if ($config['guild_require_premium'] == false || $acc_data['premdays'] > 0) {
					
						if (get_character_guild_rank($user_id) < 1 or get_character_invite($user_id) < 1) {
						
							if (preg_match("/^[a-zA-Z_ ]+$/", $_POST['guild_name'])) {
							// Only allow normal symbols as guild name
								// all is good
								$guildname = $_POST['guild_name'];
								
								create_guild($user_id, $guildname);
								header('Location: success.php');
								exit();
							} else echo 'Guild name may only contain a-z, A-Z and spaces.';
						} else echo 'You are already in a guild or invited to a guild.';
					} else echo 'You need a premium account to create a guild.';
				} else echo 'Your character must be offline to create a guild.';
			} else echo $name .' is level '. experience_to_level($char_data['experience']) .'. You need at least level '. $config['create_guild_level'] .' to create your own guild!';
		}
	}
	// end	
	?>
	
	
	<!-- FORMS TO CREATE GUILD-->
	<form action="" method="post">
		<ul>
			<li>
				Create Guild:<br>
				<select name="selected_char">
				<?php
				for ($i = 0; $i < $char_count; $i++) {
					echo '<option value="'. $characters[$i] .'">'. $characters[$i] .'</option>'; 	
				}
				?>
				</select>
				<input type="text" name="guild_name">
				
				<input type="submit" value="Create Guild">
			</li>
		</ul>
	</form>
	
	<?php
} else echo 'You need to be logged in to create a guild.';
?>
<!-- end user-->

<?php
} else { // GUILD OVERVIEW
	$gid = get_guild_id($_GET['name']);
	$inv_data = guild_invite_list($gid);
	$players = get_guild_players($gid);
	$inv_count = 0;
	
	// Calculate invite count
	if ($inv_data !== false) {
		foreach ($inv_data as $inv) {
			++$inv_count;
		}
	}

	// calculate visitor access
	$highest_access = 0;
	if (user_logged_in() === true) {
		// Get visitor access in this guild

		foreach ($players as $player) {
			$rid = $player['guild_rank'];
			
			for ($i = 0; $i < $char_count; $i++) {
				$pid = user_character_id($characters[$i]);
				if ($pid == $player['player_id']) {
					$access = get_guild_position($player['guild_rank']);
					if ($access == 2 || $access == 3) { //If player got access permissions vice leader or leader
						if ($access > $highest_access) $highest_access = $access;
					}
				}
			}
		}
	}
	// Display the specific guild page
?>

<h1>Guild: <?php echo htmlentities($_GET['name']); ?> </h1>
<table>
	<tr class="yellow">
		<td colspan="3">Guild Members</td>
	</tr>
	<tr class="yellow">
		<td>Rank</td>
		<td>Name and Title</td>
		<td>Joining Date</td>
	</tr>
		<?php
		foreach ($players as $player) {
			$chardata = user_character_data($player['player_id'], 'name', 'online');
			$color = "red";
			if ($chardata['online'] == 1)
				$color = "green";
			echo '<tr>';
			echo '<td>'. get_player_guild_rank($player['guild_rank'], $gid) .'</td>';
			echo '<td><font color="'.$color.'"><a href="characterprofile.php?name='. $chardata['name'] .'">'. $chardata['name'] .'</a> ('.$player['guild_title'].')</font></td>';
			echo '<td>'. date($config['date'], $player['joined']) .'</td>';
			echo '</tr>';
		}
		?>
</table>

<?php if ($inv_count > 0) { ?>
<h3>Invited characters</h3>
<table>
	<tr class="yellow">
		<td>Name:</td>
		<?php 
		if (user_logged_in() === true) {
			if ($highest_access == 2 || $highest_access == 3) {
				echo '<td>Remove:</td>';
			}
			// Shuffle through visitor characters
			$exist = false;
			for ($i = 0; $i < $char_count; $i++) {
				// Shuffle through invited character, see if they match your character.
				foreach ($inv_data as $inv) {
					if (user_character_id($characters[$i]) == $inv['player_id']) {
						$exist = true;
					}
				}
			}
			if ($exist) echo '<td>Join Guild:</td><td>Reject Invitation:</td>';
		}
		?>
	</tr>
		<?php
		$bool = false;
		foreach ($inv_data as $inv) {
			$uninv = user_character_data($inv['player_id'], 'name');
			echo '<tr>';
			echo '<td>'. $uninv['name'] .'</td>';
			// Remove invitation
			if ($highest_access == 2 || $highest_access == 3) {
			?> <form action="" method="post"> <?php
				echo '<td>';
				echo '<input type="hidden" name="uninvite" value="' . $inv['player_id'] . '" />';
				echo '<input type="submit" value="Remove Invitation">';
				echo '</td>';
			?> </form> <?php
			}
			// Join Guild
			?> <form action="" method="post"> <?php
			if (user_logged_in() === true) {
				for ($i = 0; $i < $char_count; $i++) {
					if (user_character_id($characters[$i]) == $inv['player_id']) {
						echo '<td>';
						echo '<input type="hidden" name="joinguild" value="' . $inv['player_id'] . '" />';
						echo '<input type="submit" value="Join Guild">';
						echo '</td>';
						$bool = true;
					}
				}
				if (!$bool && $exist) {
					echo '<td></td>';
					$bool = false;
				}
			}
			?> </form> <?php
			// Reject invitation
			?> <form action="" method="post"> <?php
			if (user_logged_in() === true) {
				for ($i = 0; $i < $char_count; $i++) {
					if (user_character_id($characters[$i]) == $inv['player_id']) {
						echo '<td>';
						echo '<input type="hidden" name="uninvite" value="' . $inv['player_id'] . '" />';
						echo '<input type="submit" value="Reject Invitation">';
						echo '</td>';
						$bool = true;
					}
				}
				if (!$bool && $exist) {
					echo '<td></td>';
					$bool = false;
				}
			}
			?> </form> <?php
			echo '</tr>';
		}
		?>
</table>
<?php } ?>
<!-- Leader stuff -->
<?php
// Only guild leaders
if (user_logged_in() === true) {
	
	// Uninvite and joinguild is also used for visitors who reject their invitation.
	if (!empty($_POST['uninvite'])) {
		//
		guild_remove_invitation($_POST['uninvite'], $gid);
		header('Location: guilds.php?name='. $_GET['name']);
		exit();
	}
	if (!empty($_POST['joinguild'])) {
		// 
		foreach ($inv_data as $inv) {
			if ($inv['player_id'] == $_POST['joinguild']) {
				$chardata = user_character_data($_POST['joinguild'], 'online');
				if ($chardata['online'] == 0) {
					if (guild_player_join($_POST['joinguild'], $gid, time())) {
						header('Location: guilds.php?name='. $_GET['name']);
						exit();
					} else echo '<font color="red" size="4">Failed to find guild position representing member.</font>';
				} else echo '<font color="red" size="4">Character must be offline before joining guild.</font>';
			}
		}
	}

	if (!empty($_POST['leave_guild'])) {
		$name = $_POST['leave_guild'];

		$cidd = user_character_id($name);
		// If character is offline
		$chardata = user_character_data($cidd, 'online');
		if ($chardata['online'] == 0) {
			guild_player_leave($cidd);
			header('Location: guilds.php?name='. $_GET['name']);
			exit();
		} else echo '<font color="red" size="4">Character must be offline first!</font>';
	}
	
if ($highest_access >= 2) {
	// Guild leader stuff
	
	// Promote character to guild position
	if (!empty($_POST['promote_character']) && !empty($_POST['promote_position'])) {
		// Verify that promoted character is from this guild.
		$p_rid = $_POST['promote_position'];
		$p_cid = user_character_id($_POST['promote_character']);
		$p_guild = get_player_guild_data($p_cid);
		
		if ($p_guild['guild_id'] == $gid) {
			// Do the magic.
			$chardata = user_character_data($p_cid, 'online');
			if ($chardata['online'] == 0) {
				update_player_guild_position($p_cid, $p_rid);
				header('Location: guilds.php?name='. $_GET['name']);
				exit();
			} else echo '<font color="red" size="4">Character not offline.</font>';
			
		}
	}
	if (!empty($_POST['invite'])) {
		if (user_character_exist($_POST['invite'])) {
			// 
			$status = false;
			foreach ($inv_data as $inv) {
				if ($inv['player_id'] == user_character_id($_POST['invite'])) $status = true;
			}
			foreach ($players as $player) {
				if ($player['name'] == $_POST['invite']) $status = true;
			}
			
			if ($status == false) {
				guild_invite_player(user_character_id($_POST['invite']), $gid);
				header('Location: guilds.php?name='. $_GET['name']);
				exit();
			} else echo '<font color="red" size="4">That character is already invited(or a member) on this guild.</font>';
		} else echo '<font color="red" size="4">That character name does not exist.</font>';
	}
	
	if (!empty($_POST['disband'])) {
		// 
		$gidd = (int)$_POST['disband'];
		$members = get_guild_players($gidd);
		$online = false;
		
		// First figure out if anyone are online.
		foreach ($members as $member) {
			$chardata = user_character_data(user_character_id($member['name']), 'online');
			if ($chardata['online'] == 1) {
				$online = true;
			}
		}
		
		if (!$online) {
			// Then remove guild rank from every player.
			foreach ($members as $member) {
				//$chardata = user_character_data(user_character_id($member['name']), 'online');
				guild_player_leave(user_character_id($member['name']));
			}
			
			// Remove all guild invitations to this guild
			if ($inv_count > 0) guild_remove_invites($gidd);
			
			// Then remove the guild itself.
			guild_delete($gidd);
			header('Location: success.php');
			exit();
		} else echo '<font color="red" size="4">All members must be offline to disband the guild.</font>';
	}
	
	if (!empty($_POST['new_leader'])) {
		$new_leader = (int)$_POST['new_leader'];
		$old_leader = guild_leader($gid);
		
		$newData = user_character_data($new_leader, 'online');
		$oldData = user_character_data($old_leader, 'online');
		
		if ($newData['online'] == 0 && $oldData['online'] == 0) { //are both leaders offline?
			if (guild_change_leader($new_leader, $old_leader, $gid)) {
				header('Location: guilds.php?name='. $_GET['name']);
				exit();
			} else echo '<font color="red" size="4">Something went wrong when attempting to change leadership.</font>';
		} else echo '<font color="red" size="4">The new and old leader must be offline to change leadership.</font>';
	}
	
	if (!empty($_POST['change_ranks'])) {
		//$c_gid = (int)$_POST['change_ranks'];
		$c_ranks = get_guild_rank_data($gid); //current guild rank information
		$n_ranks = array(); //new ranks

		// Feed new rank data
		foreach ($c_ranks as $rank) {
			if (!empty($_POST[$rank['rank_id']])) {
				$n_ranks[$rank['rank_id']] = $_POST[$rank['rank_id']];
			}
		}

		foreach ($n_ranks as $rank_id => $name) {
			guild_change_rank($rank_id, $gid, $name);
		}

		header('Location: guilds.php?name='. $_GET['name']);
		exit();
	}
	
	if (!empty($_POST['newrank'])) {
		$rname = $_POST['newrank'];
		
		create_new_rank($gid, $rname);
		header('Location: guilds.php?name='. $_GET['name']);
		exit();
	}
	
	if (!empty($_POST['remove_member'])) {
		$name = $_POST['remove_member'];
		$cid = user_character_id($name);
		
		guild_remove_member($cid);
		header('Location: guilds.php?name='. $_GET['name']);
		exit();
	}
	
	$members = count_guild_members($gid);
	$ranks = get_guild_rank_data($gid);
	?>
		<!-- forms to invite character -->
		<form action="" method="post">
			<ul>
				<li><b>Invite Character to guild:</b><br>
					<input type="text" placeholder="Character Name" name="invite">
					<input type="submit" value="Invite Character">
				</li>
			</ul>
		</form>
		<?php if ($members > 1) { ?>
		<!-- FORMS TO PROMOTE CHARACTER-->
		<form action="" method="post">
			<ul>
				<li><b>Change Character Rank:</b><br>
					<select name="promote_character">
					<?php
					//$gid = get_guild_id($_GET['name']);
					//$players = get_guild_players($gid);
					foreach ($players as $player) {
						$pl_data = get_player_guild_data($player['player_id']);
						$chardata = user_character_data($player['player_id'], 'name');
						if ($pl_data['permissions'] != 3) {
							echo '<option value="'. $chardata['name'] .'">'. $chardata['name'] .'</option>'; 
						}	
					}
					?>
					</select>
					<select name="promote_position">
						<?php
						foreach ($ranks as $rank) {
							if ($rank['permissions'] != 3) {
								if ($rank['permissions'] != 2) {
									echo '<option value="'. $rank['rank_id'] .'">'. $rank['title'] .'</option>'; 
								} else {
									if ($highest_access == 3) {
										echo '<option value="'. $rank['rank_id'] .'">'. $rank['title'] .'</option>'; 
									}
								}
							}
						}
						?>
					</select>
					<input type="submit" value="Change Rank">
				</li>
			</ul>
		</form>
		<!-- Remove member from guild -->
		<form action="" method="post">
			<ul>
				<li><b>Kick member from guild:</b><br>
					<select name="remove_member">
					<?php
					//$gid = get_guild_id($_GET['name']);
					//$players = get_guild_players($gid);
					foreach ($players as $player) {
						$pl_data = get_player_guild_data($player['player_id']);
						$chardata = user_character_data($player['player_id'], 'name');
						if ($pl_data['permissions'] != 3) {
							if ($pl_data['permissions'] != 2) {
								echo '<option value="'. $chardata['name'] .'">'. $chardata['name'] .'</option>';
							} else if ($highest_access == 3) echo '<option value="'. $chardata['name'] .'">'. $chardata['name'] .'</option>';
						}
					}
					?>
					</select>
					<input type="submit" value="Remove member">
				</li>
			</ul>
		</form>
		<?php } ?>
		<?php if ($highest_access == 3) { ?>
		<!-- forms to change guild rank titles -->
		<form action="" method="post">
			<ul>
				<li><b>Change rank titles:</b><br>
					<?php
						foreach ($ranks as $rank) {
							echo '<input type="text" name="'. $rank['rank_id'] .'" value="'. $rank['title'] .'"><br/>';
						}
						echo '<input type="hidden" name="change_ranks" value="' . $gid . '" />';
					?>
					<input type="submit" value="Update Ranks">
				</li>
			</ul>
		</form>
		<!-- forms to create a new rank -->
		<form action="" method="post">
			<ul>
				<li><b>Create new rank:</b><br>
					<input type="text" placeholder="Rank Title" name="newrank">
					<input type="submit" value="New Rank">
				</li>
			</ul>
		</form>
		<!-- forms to disband guild -->
		<form action="" method="post">
			<ul>
				<li><b>DELETE GUILD (All members must be offline):</b><br>
					<?php echo '<input type="hidden" name="disband" value="' . $gid . '" />'; ?>
					<input type="submit" value="Disband Guild">
				</li>
			</ul>
		</form>
		<!-- forms to change leadership-->
		<?php if ($members > 1) { ?>
		<form action="" method="post">
			<ul>
				<li><b>Change Leadership with:</b><br>
					<select name="new_leader">
					<?php
					//$gid = get_guild_id($_GET['name']);
					//$players = get_guild_players($gid);
					foreach ($players as $player) {
						$pl_data = get_player_guild_data($player['player_id']);
						$chardata = user_character_data($player['player_id'], 'name');
						if ($pl_data['permissions'] != 3) {
							echo '<option value="'. user_character_id($chardata['name']) .'">'. $chardata['name'] .'</option>'; 
						}	
					}
					?>
					</select>
					<input type="submit" value="Change Leadership">
				</li>
			</ul>
		</form>
		<?php }} ?>
		<?php
	}
}
?>
<!-- end leader-->
<?php
$wardata = get_guild_wars();
$war_exist = false;
if ($wardata !== false) {
	foreach ($wardata as $wars) {
		if ($wars['guild_id'] == $gid || $wars['guild_id2'] == $gid) $war_exist = true;
	}
}
if ($war_exist) {
?>
<h2>War overview:</h2>
<table>
	<tr class="yellow">
		<td>Attacker:</td>
		<td>Defender:</td>
		<td>Status:</td>
		<td>Started:</td>
	</tr>
		<?php
		foreach ($wardata as $wars) {
			if ($wars['guild_id'] == $gid || $wars['guild_id2'] == $gid) {
				$url = url("guildwar.php?warid=". $wars['war_id']);
				echo '<tr class="special" onclick="javascript:window.location.href=\'' . $url . '\'">';
				echo '<td>'. get_guild_name($wars['guild_id']) .'</td>';
				echo '<td>'. get_guild_name($wars['guild_id2']) .'</td>';
				echo '<td>'. $config['war_status'][$wars['status']] .'</td>';
				echo '<td>'. date($config['date'],$wars['started']) .'</td>';
				echo '</tr>';
			}
		}
		?>
</table>
<?php } ?>
<!-- leave guild with character -->
<?php
$bool = false;
if (user_logged_in() === true) {
	for ($i = 0; $i < $char_count; $i++) {
		foreach ($players as $player) {
			$chardata = user_character_data($player['player_id'], 'name');
			if ($chardata['name'] == $characters[$i])
				$bool = true;
		}
	}
	if ($bool) {
		?>
		<form action="" method="post">
			<ul>
				<li><b>Leave Guild:</b><br>
					<select name="leave_guild">
					<?php
					for ($i = 0; $i < $char_count; $i++) {
						foreach ($players as $player) {
							$chardata = user_character_data($player['player_id'], 'name');
							if ($chardata['name'] == $characters[$i]) {
								$data = get_player_guild_data(user_character_id($chardata['name']));
								if ($data['permissions'] != 3)
									echo '<option value="'. $characters[$i] .'">'. $characters[$i] .'</option>';
							}
						}
					}
					?>
					</select>
					<input type="submit" value="Leave Guild">
				</li>
			</ul>
		</form>
		<?php
	} // display form if user has a character in guild
} // user logged in
} // if warname as $_GET
include 'layout/overall/footer.php'; ?>