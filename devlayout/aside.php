<aside>
	<?php 
		if (user_logged_in() === true) {
			include 'layout/widgets/loggedin.php'; 
		} else {
			include 'layout/widgets/login.php'; 
		}
		include 'layout/widgets/charactersearch.php';
		include 'layout/widgets/highscore.php';
		include 'layout/widgets/serverinfo.php';
	?>
</aside>