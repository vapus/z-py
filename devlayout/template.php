<!doctype html>
<html>
<head>
	<title>Znote AAC</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="design.css">
</head>
<body>
	<header>
		<h1 class="logo">Znote AAC</h1>
		<nav>
			<ul>
				<li><a href="index.php">Home</a></li>
				<li><a href="downloads.php">Downloads</a></li>
				<li><a href="forum.php">Forum</a></li>
				<li><a href="contact.php">Contact us</a></li>
			</ul>
		</nav>
		<div class="clear"></div>
	</header>
	<div id="container">
		<aside>
			<div class="widget">
				<h2>Widget Header</h2>
				<div class="inner">
					Widget contents
				</div>
			</div>
			<div class="widget">
				<h2>Widget Header</h2>
				<div class="inner">
					Widget contents
				</div>
			</div>
		</aside>
	</div>
	<footer>
		&copy; phpacademy.org 2011. All rights reserved.
	</footer>
</body>
</html>