<?php require_once 'engine/init.php'; include 'layout/overall/header.php'; ?>

<h1>Who is online?</h1>
<?php
$array = online_list();
if ($array) {
	?>
	
	<table>
		<tr class="yellow">
			<td>Name:</td>
			<td>Level:</td>
			<td>Vocation:</td>
			<td>World:</td>
		</tr>
			<?php
			foreach ($array as $value) {
			echo '<tr>';
			echo '<td><a href="characterprofile.php?name='. urlencode($value['name']) .'">'. $value['name'] .'</a></td>';
			echo '<td>'. experience_to_level($value['experience']) .'</td>';
			echo '<td>'. $config['vocations'][$value['vocation']] .'</td>';
			echo '<td>'. world_id_to_name($value['world_id']) .'</td>';
			echo '</tr>';
			}
			?>
	</table>

	<?php
} else {
	echo 'Nobody is online.';
}
?>
<?php include 'layout/overall/footer.php'; ?>