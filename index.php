 <?php
 require_once 'engine/init.php';
 include 'layout/overall/header.php';

echo '<h1>News</h1>';

$news_array = get_news_list();
foreach($news_array as $news) {
    echo '<table border="0" cellspacing="5"><tr>';
    echo '<td><strong><p>'.$news['title'].'</strong><br/>- posted on '.date($config['date'], $news['created']).'</p></tr></td>';
    echo '<td>'.$news['text'].'</td></tr></table>';
}

include 'layout/overall/footer.php';
?> 