 <?php
 require_once 'engine/init.php';
 include 'layout/overall/header.php';
 
echo '<h1>Houses</h1>';
echo 'Here you can see the list of all available houses, flats or guildhalls. Please select the game
world of your choice. Click on any view button to get more information about a house or adjust
the search criteria and start a new search. <bR><bR><bR>';

//post not empty
if (empty($_POST) === false) {
	//if (!empty($_POST['world']) && !empty($_POST['town']) && !empty($_POST['state']) && !empty($_POST['order']) && !empty($_POST['style'])) {
		echo '<table bORDER=0 CELLSPACING=1 CELLPADDING=4 width=100%>
		<tr><td COLSPAN=6><b>Available Houses and Flats in '.town_id_to_name($_POST['town']).' on '.world_id_to_name($_POST['world']).'</b></td></tr>
		<tr>
		  <td width=40%><b>Name</b></td>
		  <td width=10%><b>Size</b></td>
		  <td width=10%><b>Rent</b></td>
		  <td width=40%><b>Status</b></td>
		</tr>';

		$house_array = get_house_list($_POST['order'], $_POST['town'], $_POST['state']);
		foreach ($house_array as $house) {
			echo '<tr>
			  <td width=40%><b>'.$house['name'].'</b></td>
			  <td width=10%><b>'.$house['size'].'</b></td>
			  <td width=10%><b>'.$house['rent'].'</b></td>
			  <td width=40%><b>'.$house['owner'].'</b></td>
			</tr>';
		}

		echo '</td></tr></table>';
	//}
}


echo '<FORM ACTION="" METHOD=post><table bORDER=0 CELLSPACING=1 CELLPADDING=4 width=100%>
<tr><td COLSPAN=4><b>House Search</b></td></tr>
<tr><td width=25%><b>World</b></td><td width=25%><b>Town</b></td><td width=25%><b>Status</b></td><td width=25%><b>Order</b></td></tr>
<tr><td VALIGN=top ROWSPAN=2>
<SELECT SIZE="1" name="world">';
	foreach ($config['worlds'] as $a => $k) {
		echo '<OPTION value="'.$a.'">'.$k.'</OPTION>';
	}
echo '</SELECT>
</td><td VALIGN=top ROWSPAN=2>';
	foreach ($config['towns'] as $a => $k) {
		echo '<input type=radio name="town" value="'.$a.'" CHECKED>'.$k.'<bR>';
	}
echo'</td><td VALIGN=top>
  <input type=radio name="state" value="" CHECKED> all states<bR>
  <input type=radio name="state" value="0"> auctioned<bR>
  <input type=radio name="state" value="1"> rented<bR>
</td><td VALIGN=top ROWSPAN=2>
  <input type=radio name="order" value="name" CHECKED> by name<bR>
  <input type=radio name="order" value="size"> by size<bR>
  <input type=radio name="order" value="rent"> by rent<bR>
  <input type=radio name="order" value="price"> by bid<bR>';
  //<input type=radio name="order" value="end"  > by auction end<bR>
echo '</td></tr>
<tr bGCOLOR=#D4C0A1><td VALIGN=top>
  <input type=radio name="style" value="houses" CHECKED> houses and flats<bR>';
  //<input type=radio name="style" value="guildhalls" > guildhalls<bR>
echo '</td></tr>
</table>';

echo '<center><input name="Sumbit" type="submit" value="Submit"></FORM></center>';

include 'layout/overall/footer.php';
?> 