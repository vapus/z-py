<?php
require_once 'engine/init.php';
include 'layout/overall/header.php';
protect_page();
admin_only($user_data);

if (empty($_GET['edit'])) {
	// Display the lastest new items
	echo '<h1>News List:</h1>';

	$news_array = get_news_list();
	if (is_array($news_array) and count($news_array) > 0) {
	?>
		<form action="" method="get">
		<table>
			<tr class="yellow"><td><center>
				<input type="submit" name="" value=" Edit Selected News ">
			</td></tr>
		<?php
			foreach($news_array as $news) {
				echo '<tr><td><input type="radio" name="edit" value="'.$news['id'].'"><b> '.$news['title'].'</b> posted on '.date($config['date'], $news['created']). '</td></tr>';
			}
		?>
		</table></form>
	<?php } else echo '<p>There is no news.</p>';

	if (user_logged_in() === true) {
		// post verifications
		// CREATE GUILD
		if (!empty($_POST['title']) && !empty($_POST['body'])) {
				//code here
				if (preg_match("/^[a-zA-Z_ ]+$/", $_POST['title'])) {
					$title = $_POST['title'];
					$body = $_POST['body'];
					$created = time();
					
					add_news($title, $body, $created);
					header('Location: success.php');
					exit();
				} else echo 'News title name may only contain a-z, A-Z and spaces.';
		}
		// end	
		?>

		<!-- FORM TO ADD NEWS -->
		<form action="" method="post">
		<table>
			<tr class="yellow"><td>Add news panel</td></tr>
			<tr><td><input type="text" name="title" size="50" placeholder="News Title"/></td></tr>
			<tr><td><textarea name="body" rows="30" cols="80" style="resize:none" placeholder="News body ( you can use PHP tags )"/></textarea>
			<tr><td><center><input type="submit" name="add" value="Add news"></center></td></tr>
		</table>
		</form>
		
		<?php
	} else echo 'You need to be logged in to add news.';

} else { // GUILD OVERVIEW
	$newsitem = get_news_by_id($_GET['edit']);

	echo '<table>
		<tr class="yellow"><td>Edit news panel</td></tr>
		<form action="" method="post">
		<input type="hidden" name="nid" value="' . $newsitem['id'] . '" />
		<tr><td><input type="text" name="etitle" size="50" value="'.$newsitem['title'].'"/></td></tr>
		<tr><td><textarea name="ebody" rows="30" cols="80" style="resize:none""/>'.$newsitem['text'].'</textarea>
		<tr><td><center><input type="submit" name="submit" value="Edit news"></center></td></tr>
		</form>
		<form action="" method="post">
		<input type="hidden" name="id" value="' . $newsitem['id'] . '" />
		<tr><td><center><input type="submit" name="delete" value="Delete news"></center></td></tr>
		</form>
	</table>';
	// Display the specific guild page

	// making sure we are logged in
	if (user_logged_in() === true) {
		
		// Uninvite and joinguild is also used for visitors who reject their invitation.
		if (!empty($_POST['delete'])) {
			//
			guild_remove_invitation($_POST['uninvite'], $gid);
			delete_news($_POST['id']);
			header('Location: news.php');
			exit();
		}
		if (!empty($_POST['etitle']) and !empty($_POST['ebody']) and !empty($_POST['nid'])) {
			// 
			edit_news($_POST['etitle'], $_POST['ebody'], $_POST['nid']);
			header('Location: news.php');
			exit();
		}
	}
}

include 'layout/overall/footer.php';?>