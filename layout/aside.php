<div id="sidebar_container">
	<?php 
		if (user_logged_in() === true) {
			include 'layout/widgets/loggedin.php'; 
		} else {
			include 'layout/widgets/login.php'; 
		}
		include 'layout/widgets/charactersearch.php';
		include 'layout/widgets/highscore.php';
		include 'layout/widgets/serverinfo.php';
		// Remove // to enable twitter, edit twitter stuff in /widgets/twitter.php
		//include 'layout/widgets/twitter.php';
	?>
</div>