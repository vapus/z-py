<?php

// Generate recovery key
function generate_recovery_key($lenght) {
	$lenght = (int)$lenght;
	$tmp = rand(1000, 9000);
	$tmp += time();
	$tmp = sha1($tmp);
	
	$results = '';
	for ($i = 0; $i < $lenght; $i++) $results = $results.''.$tmp[$i];
	
	return $results;
}

// Calculate discount
function calculate_discount($orig, $new) {
	$orig = (int)$orig;
	$new = (int)$new;
	
	$tmp = '';
	if ($new >= $orig) {
		if ($new != $orig) {
			$calc = ($new/$orig) - 1;
			$calc *= 100;
			$tmp = '+'. $calc .'%';
		} else $tmp = '0%';
	} else {
		$calc = 1 - ($new/$orig);
		$calc *= 100;
		$tmp = '-'. $calc .'%';
	}
	return $tmp;
}

// Proper URLs
function url($path = false) {
	$protocol = (!empty($_SERVER['HTTPS']) ? 'https' : 'http') . '://';
	$domain   = $_SERVER['SERVER_NAME'] . ($_SERVER['SERVER_PORT'] != 80 ? ':' . $_SERVER['SERVER_PORT'] : null);
	$folder   = dirname($_SERVER['SCRIPT_NAME']);

	return $protocol . $domain . $folder . '/' . $path;
}

// Get last cached
function getCache() {
	$value = array();
	try{
		$result = $GLOBALS['dbh']->query("SELECT `cached` FROM `znote`;");
		$value = $result->fetch(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		$value['cached'] = false;
	}

	return $value['cached'];
}

function setCache($time) {
	$time = (int)$time;
	$GLOBALS['dbh']->exec("UPDATE `znote` set `cached`='$time'");
}

// Get visitor basic data
function znote_visitors_get_data() {
	// select
	try{
		$result = $GLOBALS['dbh']->query("SELECT `ip`, `value` FROM `znote_visitors`");
		$data = $result->fetchAll(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		$data = false;
	}
	return $data;
}

// Set visitor basic data
function znote_visitor_set_data($visitor_data) {
	$exist = false;
	$ip = ip2long(getIP());
	
	foreach ((array)$visitor_data as $row) {
		if ($ip == $row['ip']) {
			$exist = true;
			$value = $row['value'];
		}
	}
	
	if ($exist && isset($value)) {
		// Update the value
		$value++;
		$GLOBALS['dbh']->exec("UPDATE `znote_visitors` SET `value` = '$value' WHERE `ip` = '$ip'");
	} else {
		// Insert new row
		$GLOBALS['dbh']->exec("INSERT INTO `znote_visitors` (`ip`, `value`) VALUES ('$ip', '1')");
	}
}

// Get visitor basic data
function znote_visitors_get_detailed_data($cache_time) {
	$period = (int)time() - (int)$cache_time;
	// select
	try{
		$result = $GLOBALS['dbh']->query("SELECT `ip`, `time`, `type`, `account_id` FROM `znote_visitors_details` WHERE `time` >= '$period' LIMIT 0, 50");
		$data = $result->fetchAll(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		$data = false;
	}
	return $data;
}

function znote_visitor_insert_detailed_data($type) {
	$type = (int)$type;
	/*
	type 0 = normal visits
	type 1 = register form
	type 2 = character creation
	type 3 = fetch highscores
	type 4 = search character
	*/
	$time = time();
	$ip = ip2long(getIP());
	if (user_logged_in() === true) {
		$acc = $_SESSION['user_id'];
		$GLOBALS['dbh']->exec("INSERT INTO `znote_visitors_details` (`ip`, `time`, `type`, `account_id`) VALUES ('$ip', '$time', '$type', '$acc')");
	} else {
		$GLOBALS['dbh']->exec("INSERT INTO `znote_visitors_details` (`ip`, `time`, `type`, `account_id`) VALUES ('$ip', '$time', '$type', '0')");
	}
}

function something () {
	// Make acc data compatible:
	$ip = ip2long(getIP());
}

// Secret token
function create_token() {
	echo 'Checking whether to create token or not<br />';
	#if (empty($_SESSION['token'])) {
		echo 'Creating token<br />';
		$token = sha1(uniqid(time(), true));
		$token2 = $token;
		var_dump($token, $token2);
		$_SESSION['token'] = $token2;
	#}
	
	echo "<input type=\"hidden\" name=\"token\" value=\"". $_SESSION['token'] ."\" />";
}
function reset_token() {
	echo 'Reseting token<br />';
	unset($_SESSION['token']);
}

// Time based functions
// 60 seconds to 1 minute
function second_to_minute($seconds) {
	return ($seconds / 60);
}

// 1 minute to 60 seconds
function minute_to_seconds($minutes) {
	return ($minutes * 60);
}

// 60 minutes to 1 hour
function minute_to_hour($minutes) {
	return ($minutes / 60);
}

// 1 hour to 60 minutes
function hour_to_minute($hours) {
	return ($hour * 60);
}

// seconds / 60 / 60 = hours.
function seconds_to_hours($seconds) {
	$minutes = second_to_minute($seconds);
	$hours = minute_to_hour($minutes);
	return $hours;
}

function remaining_seconds_to_clock($seconds) {
	return date("(H:i)",time() + $seconds);
}

// Returns false if name contains more than configured max words, returns name otherwise.
function validate_name($string) {
	//edit: make sure only one space separates words: 
	//(found this regex through a search and havent tested it)
	$string  = preg_replace("/\\s+/", " ", $string);

	//trim off beginning and end spaces;
	$string = trim($string);

	//get an array of the words
	$wordArray = explode(" ", $string);

	//get the word count
	$wordCount = sizeof($wordArray);

	//see if its too big
	if($wordCount > config('maxW')) {
		return false;
	} else {
		return $string;
	}
}

// Checks if an IPv4 address is valid
function validate_ip($ip) {
	$ipL = ip2long($ip);
	$ipR = long2ip($ipL);
	
	if ($ip === $ipR) {
		return true;
	} else {
		return false;
	}
}

// Fetch a config value. Etc config('vocations') will return vocation array from config.php.
function config($value) {
	global $config;
	return $config[$value];
}

// Some functions uses several configurations from config.php, so it sounds
// smarter to give them the whole array instead of calling the function all the time.
function fullConfig() {
	global $config;
	return $config;
}

// Capitalize Every Word In String.
function format_character_name($name) {
	return ucwords(strtolower($name));
}

// Returns a list of players online
function online_list() {
	try{
		$query = $GLOBALS['dbh']->query("SELECT `name`, `experience`, `vocation`, `world_id` FROM `players` WHERE `online`='1' ORDER BY `name` DESC;");
		$online = $query->fetchAll(PDO::FETCH_ASSOC);
	} catch (PDOException $e) {
		$online = false;
	}
	return $online;
}

// Gets you the actual IP address even from users behind ISP proxies and so on.
function getIP() {
  $IP = '';
  if (getenv('HTTP_CLIENT_IP')) {
    $IP =getenv('HTTP_CLIENT_IP');
  } elseif (getenv('HTTP_X_FORWARDED_FOR')) {
    $IP =getenv('HTTP_X_FORWARDED_FOR');
  } elseif (getenv('HTTP_X_FORWARDED')) {
    $IP =getenv('HTTP_X_FORWARDED');
  } elseif (getenv('HTTP_FORWARDED_FOR')) {
    $IP =getenv('HTTP_FORWARDED_FOR');
  } elseif (getenv('HTTP_FORWARDED')) {
    $IP = getenv('HTTP_FORWARDED');
  } else {
    $IP = $_SERVER['REMOTE_ADDR'];
  }
return $IP;
}

function array_length($ar) {
	$r = 1;
	foreach($ar as $a) {
		$r++;
	}
	return $r;
}

// Parameter: experience returns: level
function experience_to_level($experience) {
	$l1 = pow((pow(3, 0.5) * (pow(((243 * pow($experience, 2)) - (48600 * $experience) + 3680000), 0.5)) + (27 * $experience) - 2700), 1.0 / 3);
    $l2 = pow(30, 2.0/3);
    $l3 = 5 * pow(10, 2.0/3);
    $l4 = pow(3, 1.0/3) * $l1;

    return floor(($l1/$l2)-($l3/$l4)+2);
}

// Parameter: tries, vocation id, and skill returns: skill level, (works for fist,sword, club, axe, distance) distance?
//NOT CURRENTLY IN USE
function tries_to_skill($tries, $voc_id, $skillid) {
	$skill_multipliers = config('skill_multipliers');
	$skill = explode ('_', $skillid);
	$skill = $skill[0];
	
	$s1 = ($tries * ($skill_multipliers[$voc_id][$skill] - 1)/50) + 1;
	$s2 = log($skill_multipliers[$voc_id][$skill]);
    return floor(($s1/$s2)+10);
}

// Parameter: tries, vocation id returns: fishing level
//NOT CURRENTLY IN USE
function tries_to_fishing($tries, $voc_id, $skillid) {
	$skill_multipliers = config('skill_multipliers');
	$skill = explode ('_', $skillid);
	$skill = $skill[0];

	$f1 = ($tries + 200)/200;
	$f2 = log($skill_multipliers[$voc_id][$skill]);
	return floor(($f1/$f2)+10);
}

// Parameter: manaspent, vocation id returns: magic level
function manaspent_to_ml($mana, $voc_id) {
	$skill_multipliers = config('skill_multipliers');

	$ml1 = ($mana * ($skill_multipliers[$voc_id]['magic'] - 1)/1600) + 1;
	return floor(log($ml1, $skill_multipliers[$voc_id]['magic']));
}

// Parameter: skill id returns matching db column name
function skillid_to_column_name($id) {
	$skill_db = array(
		0 => 'fist_tries',
		1 => 'club_tries',
		2 => 'sword_tries',
		3 => 'axe_tries',
		4 => 'distance_tries',
		5 => 'shield_tries',
		6 => 'fishing_tries'
	);
	
	return ($skill_db[$id] >= 0) ? $skill_db[$id] : false;
}

// Parameter: players.hide_char returns: Status word inside a font with class identifier so it can be designed later on by CSS.
function hide_char_to_name($id) {
	$id = (int)$id;
	if ($id == 1) {
		return 'hidden';
	} else {
		return 'visible';
	}
}

// Parameter: players.online returns: Status word inside a font with class identifier so it can be designed later on by CSS.
function online_id_to_name($id) {
	$id = (int)$id;
	if ($id == 1) {
		return '<font class="status_online">ONLINE</font>';
	} else {
		return '<font class="status_offline">offline</font>';
	}
}

// Parameter: players.vocation_id. Returns: Configured vocation name.
function vocation_id_to_name($id) {
	$vocations = config('vocations');

	return (array_key_exists($id, $vocations)) ? $vocations[$id] : false;
}

function gender_exist($gender) {
	// Range of allowed gender ids, fromid toid
	if ($gender >= 0 && $gender <= 1) {
		return true;
	} else {
		return false;
	}
}

function skillid_to_name($skillid) {
	$skillname = array(
		0 => 'fist fighting',
		1 => 'club fighting',
		2 => 'sword fighting',
		3 => 'axe fighting',
		4 => 'distance fighting',
		5 => 'shielding',
		6 => 'fishing',
		7 => 'experience', // Hardcoded, does not actually exist in database as a skillid.
		8 => 'magic level' // Hardcoded, does not actually exist in database as a skillid.
	);

	return ($skillname[$skillid] >= 0) ? $skillname[$skillid] : false;
}

// Parameter: players.town_id. Returns: Configured town name.
function town_id_to_name($id) {
	$towns = config('towns');
	
	return (array_key_exists($id,$towns)) ? $towns[$id] : false;
}

// Parameter: players.world_id. Returns: Configured town name.
function world_id_to_name($id) {
	$worlds = config('worlds');
	
	return ($worlds[$id] >= 0) ? $worlds[$id] : false;
}

// Unless you have an internal mail server then mail sending will not be supported in this version.
function email($to, $subject, $body) {
	mail($to, $subject, $body, 'From: TEST');
}

function logged_in_redirect() {
	if (user_logged_in() === true) {
		header('Location: myaccount.php');
	}
}

function protect_page() {
	if (user_logged_in() === false) {
		header('Location: protected.php');
		exit();
	}
}

// When function is called, you will be redirected to protect_page and deny access to rest of page, as long as you are not admin.
function admin_only($user_data) {	
	// Chris way
	$gotAccess = is_admin($user_data);
	
	if ($gotAccess == false) {
		logged_in_redirect();
		exit();
	}
}

function is_admin($user_data) {
	return in_array($user_data['name'], config('page_admin_access')) ? true : false;
}

function output_errors($errors) {
	return '<ul><li>'. implode('</li><li>', $errors) .'</li></ul>';
}
?>