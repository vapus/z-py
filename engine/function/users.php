<?php
// Shop
// Gets a list of tickets and ticket ids
function shop_delete_row_order($rowid) {
	$rowid = (int)$rowid;
	try{
		return $GLOBALS['dbh']->exec("DELETE FROM `znote_shop_orders` WHERE `id`=$rowid;");
	} catch(PDOException $e) {
    	die($e->getMessage());
	}
}

function shop_update_row_count($rowid, $count) {
	$rowid = (int)$rowid;
	$count = (int)$count;
	try{
		return $GLOBALS['dbh']->exec("UPDATE `znote_shop_orders` SET `count`=$count WHERE `id`=$rowid");
	} catch(PDOException $e) {
    	die($e->getMessage());
	}
}

function shop_account_gender_tickets($accid) {
	$accid = (int)$accid;
	$array = $GLOBALS['dbh']->query("SELECT `id`, `count` FROM `znote_shop_orders` WHERE `account_id`=$accid AND `type`='3';")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:false;
}

// GUILDS
//
function guild_remove_member($cid) {
	$cid = (int)$cid;
	try{
		return $GLOBALS['dbh']->exec("DELETE FROM `player_guild` WHERE `player_id`=$cid;");
	} catch(PDOException $e) {
    	die($e->getMessage());
	}
}

function guild_change_rank($rid, $gid, $name) {
	$rid = (int)$rid;
	$gid = (int)$gid;
	try{
		$sth = $GLOBALS['dbh']->prepare("UPDATE `guild_ranks` SET `title`=:name WHERE `rank_id`=:rankid AND `guild_id`=:guildid ;");
		$sth->bindValue(':name', $name, PDO::PARAM_STR);
		$sth->bindValue(':rankid', $rid, PDO::PARAM_INT);
		$sth->bindValue(':guildid', $gid, PDO::PARAM_INT);
		$sth->execute();
	} catch(PDOException $e) {
    	die($e->getMessage());
	}
}

function create_new_rank($gid, $rname) {
	$gid = (int)$gid;
	$query = $GLOBALS['dbh']->query("SELECT MAX(`rank_id`) FROM `guild_ranks` WHERE `guild_id`=$gid;")->fetch(PDO::FETCH_NUM);
	$curmax = $query[0];
	$newmax = $curmax + 1;
	try{
		$sth = $GLOBALS['dbh']->prepare("INSERT INTO `guild_ranks` (`guild_id`, `rank_id`, `title`, `permissions`) VALUES (:guildid, :rankid, :title, :permissions)");
		$sth->bindValue(':guildid', $gid, PDO::PARAM_STR);
		$sth->bindValue(':rankid', $newmax, PDO::PARAM_INT);
		$sth->bindValue(':title', $rname, PDO::PARAM_INT);
		$sth->bindValue(':permissions', 1, PDO::PARAM_INT);
		$sth->execute();
	} catch(PDOException $e) {
    	die($e->getMessage());
	}
}
// Change guild leader (parameters: cid, new and old leader).
function guild_change_leader($nCid, $oCid, $gid) {
	$nCid = (int)$nCid;
	$oCid = (int)$oCid;
	$gid = (int)$gid;
	$ranks = get_guild_rank_data($gid);
	
	$leader_rid = 0;
	$vice_rid = 0;
	
	
	// Get rank id for leader and vice leader.
	foreach ($ranks as $rank) {
		if ($rank['permissions'] == 3) $leader_rid = $rank['rank_id'];
		if ($rank['permissions'] == 2) $vice_rid = $rank['rank_id'];
	}
	
	$status = false;
	if ($leader_rid > 0 && $vice_rid > 0) $status = true;
	
	// Verify that we found the rank ids for vice leader and leader.
	if ($status) {
		// Update players and set their new rank id
		try{
			$GLOBALS['dbh']->beginTransaction();
			$GLOBALS['dbh']->exec("UPDATE `player_guild` SET `guild_rank`=$leader_rid WHERE `player_id`=$nCid");
			$GLOBALS['dbh']->exec("UPDATE `player_guild` SET `guild_rank`=$vice_rid WHERE `player_id`=$oCid");
			$GLOBALS['dbh']->commit();
		} catch(PDOException $e) {
			$GLOBALS['dbh']->rollback();
			die($e->getMessage());
		}
	}
	
	return $status;
}
function guild_leader($gid) {
	$gid = (int)$gid;
	if($result = $GLOBALS['dbh']->query("SELECT `player_id` FROM `player_guild` WHERE `guild_id`=$gid AND `guild_rank`='1';")->fetch(PDO::FETCH_NUM)){
		return $result[0];
	} else{
		return 0;
	}
}

// Remove guild invites
function guild_remove_invites($gid) {
	$gid = (int)$gid;
	$GLOBALS['dbh']->exec("DELETE FROM `guild_invites` WHERE `guild_id`='$gid';");
}

// Disband guild
function guild_delete($gid) {
	$gid = (int)$gid;
	$GLOBALS['dbh']->exec("DELETE FROM `guilds` WHERE `guild_id`='$gid';");
	$GLOBALS['dbh']->exec("DELETE FROM `player_guild` WHERE `guild_id`=$gid");
	$GLOBALS['dbh']->exec("DELETE FROM `guild_invites` WHERE `guild_id`='$gid';");
	return true;
}

// Player leave guild
function guild_player_leave($cid) {
	$cid = (int)$cid;
	$GLOBALS['dbh']->exec("DELETE FROM `player_guild` WHERE `player_id`='$cid';");
}

// Player join guild
function guild_player_join($cid, $gid, $time) {
	// Get rank data
	$ranks = get_guild_rank_data($gid);
	
	// Locate rank id for regular member position in this guild
	$rid = false;
	foreach ($ranks as $rank) {
		if ($rank['permissions'] == 1)
			$rid = $rank['rank_id'];
	}
	
	$cid = (int)$cid;
	
	// Create a status we can return depending on results.
	$status = false;
	
	// Add to guild if rank id was found:
	if ($rid != false) {
		// Remove the invite:
		guild_remove_invitation($cid, $gid);
		
		// Add to guild:
		try{
			return $GLOBALS['dbh']->exec("INSERT INTO `player_guild` (`player_id`, `guild_id`, `guild_rank`, `guild_title`, `joined`) VALUES ('$cid', '$gid', '$rid', '', '$time')");
		} catch(PDOException $e) {
			die($e->getMessage());
		}
		// Delete old invite
		$GLOBALS['dbh']->exec("DELETE FROM `guild_invites` WHERE `player_id`='$cid' AND `guild_id`='$gid';");
		$status = true;
	}
	
	return $status;
}

// Remove cid invitation from guild (gid)
function guild_remove_invitation($cid, $gid) {
	$cid = (int)$cid;
	$gid = (int)$gid;
	$GLOBALS['dbh']->exec("DELETE FROM `guild_invites` WHERE `player_id`='$cid' AND `guild_id`='$gid';");
}

// Invite character to guild
function guild_invite_player($cid, $gid) {
	$cid = (int)$cid;
	$gid = (int)$gid;
	try{
		return $GLOBALS['dbh']->exec("INSERT INTO `guild_invites` (`player_id`, `guild_id`) VALUES ('$cid', '$gid')");
	} catch(PDOException $e) {
		die($e->getMessage());
	}
}

// Gets a list of invited players to a particular guild.
function guild_invite_list($gid) {
	$gid = (int)$gid;
	$array = $GLOBALS['dbh']->query("SELECT `player_id`, `guild_id` FROM `guild_invites` WHERE `guild_id`='$gid'")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))? $array : false;
}

// Update player's guild position
function update_player_guild_position($cid, $rid) {
	$cid = (int)$cid;
	$rid = (int)$rid;
	try{
		return $GLOBALS['dbh']->exec("UPDATE `player_guild` SET `guild_rank`='$rid' WHERE `player_id`=$cid");
	} catch(PDOException $e) {
		die($e->getMessage());
	}
}

// Get guild data, using guild id.
function get_guild_rank_data($gid) {
	$gid = (int)$gid;
	$array = $GLOBALS['dbh']->query("SELECT * FROM `guild_ranks` WHERE `guild_id`='$gid' ORDER BY `permissions` DESC LIMIT 0, 30")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))? $array : false;
}

// Creates a guild, where cid is the owner of the guild, and name is the name of guild.
function create_guild($cid, $name) {
	$cid = (int)$cid;
	$world_id = user_character_world_id($cid);
	$time = time();
	
	// Create the guild
	try{
		$GLOBALS['dbh']->beginTransaction();
		$sth = $GLOBALS['dbh']->prepare("INSERT INTO `guilds` (`world_id`,`name`, `created`, `motd`) VALUES (:worldid, :name, :time, :motd)");
		$sth->bindValue(':worldid', $world_id, PDO::PARAM_INT);
		$sth->bindValue(':name', $name, PDO::PARAM_STR);
		$sth->bindValue(':time', $time, PDO::PARAM_INT);
		$sth->bindValue(':motd', 'The guild has been created!', PDO::PARAM_STR);
		$sth->execute();
		// Get guild id
		$gid = $GLOBALS['dbh']->lastInsertId(); //$gid = get_guild_id($name);
		
		//created leader rank
		$GLOBALS['dbh']->exec("INSERT INTO `guild_ranks` (`guild_id`, `rank_id`, `title`, `permissions`) VALUES ('$gid', '1', 'Leader', '3')");
		$GLOBALS['dbh']->exec("INSERT INTO `guild_ranks` (`guild_id`, `rank_id`, `title`, `permissions`) VALUES ('$gid', '2', 'Vice Leader', '2')");
		$GLOBALS['dbh']->exec("INSERT INTO `guild_ranks` (`guild_id`, `rank_id`, `title`, `permissions`) VALUES ('$gid', '3', 'Member', '1')");
		$GLOBALS['dbh']->commit();
	} catch(PDOException $e) {
		$GLOBALS['dbh']->rollback();
		die($e->getMessage());
	}
		echo '<br>Created guild.';
		echo '<br>Gotten guild id: '. $gid;
	//add player to the guild at the leader rank
	try{
		$GLOBALS['dbh']->exec("INSERT INTO `player_guild` (`player_id`, `guild_id`, `guild_rank`, `guild_title`, `joined`) VALUES ('$cid', '$gid', '1', '', '$time')");
	} catch(PDOException $e) {
		die($e->getMessage());
	}

}

// Search player table on cid for his rank_id, returns rank_id
function get_character_guild_rank($cid) {
	$cid = (int)$cid;

	if($rid = $GLOBALS['dbh']->query("SELECT `guild_rank` FROM `player_guild` WHERE `player_id`='$cid';")->fetch(PDO::FETCH_NUM)){
		return $rid[0];
	}else{
		return false;
	}
}

// Search player table on cid for his rank_id, returns rank_id
function get_character_invite($cid) {
	$cid = (int)$cid;

	if($gid = $GLOBALS['dbh']->query("SELECT `guild_id` FROM `guild_invites` WHERE `player_id`='$cid';")->fetch(PDO::FETCH_NUM)){
		return ($gid[0]>0)?$gid[0]:false;
	} else { 
		return false;
	}
}

// Get a player guild rank, using his rank_id
function get_player_guild_rank($rank_id, $guild_id) {
	$rank_id = (int)$rank_id;
	$guild_id = (int)$guild_id;
	if($result = $GLOBALS['dbh']->query("SELECT `title` FROM `guild_ranks` WHERE `rank_id`=$rank_id AND `guild_id`=$guild_id ;")->fetch(PDO::FETCH_NUM)){
		return $result[0];
	} else {
		return false;
	}
}

// Get a player guild position ID, using his rank_id
function get_guild_position($rid) {
	$rid = (int)$rid;
	if($result = $GLOBALS['dbh']->query("SELECT `permissions` FROM `guild_ranks` WHERE `rank_id`=$rid;")->fetch(PDO::FETCH_NUM)){
		return $result[0];
	} else {
		return false;
	}
}

// Get a players rank_id, guild_id, rank_level(ID), rank_name(string), using cid(player id)
function get_player_guild_data($cid) {
	$cid = (int)$cid;
	$rid = $GLOBALS['dbh']->query("SELECT `guild_rank` FROM `player_guild` WHERE `player_id`='$cid';")->fetchColumn();
	$gid = $GLOBALS['dbh']->query("SELECT `guild_id` FROM `player_guild` WHERE `player_id`='$cid';")->fetchColumn();
	$rl = $GLOBALS['dbh']->query("SELECT `permissions` FROM `guild_ranks` WHERE `rank_id`=$rid;")->fetchColumn();
	$rn = $GLOBALS['dbh']->query("SELECT `title` FROM `guild_ranks` WHERE `rank_id`=$rid AND `guild_id`=$gid;")->fetchColumn();
	$data = array(
		'rank_id' => $rid,
		'guild_id' => $gid,
		'permissions' => $rl,
		'rank_name' => $rn,
	);
	return $data;
}

// Returns guild name of guild id
function get_guild_name($gid) {
	$gid = (int)$gid;
	$result = $GLOBALS['dbh']->query("SELECT `name` FROM `guilds` WHERE `guild_id`=$gid;")->fetch(PDO::FETCH_NUM);
	return $result[0];
}

// Returns guild id from name
function get_guild_id($name) {
	$stmt = $GLOBALS['dbh']->prepare("SELECT `guild_id` FROM `guilds` WHERE `name`=?;");
	if ($stmt->execute(array($name))) {
		if($row = $stmt->fetch(PDO::FETCH_NUM)) {
			return $row[0];
		}
	}
}

// Get complete list of guilds
function get_guilds_list() {
	$array = $GLOBALS['dbh']->query("SELECT `guild_id`, `name`, `created` FROM `guilds` ORDER BY `name`;")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:false;
}

// Get array of player data related to a guild.
function get_guild_players($gid) {
	$gid = (int)$gid; // Sanitizing the parameter id
	$array = $GLOBALS['dbh']->query("SELECT * FROM `player_guild` WHERE `guild_id`='$gid' ORDER BY `guild_rank`;")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:array();
}

// Returns total members in a guild (integer)
function count_guild_members($gid) {
	$gid = (int)$gid;
	//return mysql_result(mysql_query("SELECT COUNT(p.id) AS total FROM players AS p LEFT JOIN guild_ranks AS gr ON gr.id = p.rank_id WHERE gr.guild_id =$gid"), 0, 'total');
	if($result = $GLOBALS['dbh']->query("SELECT COUNT('*') FROM `player_guild` WHERE `guild_id`='$gid'")->fetchColumn())
		return $result;
}

//
// GUILD WAR
//
// Returns guild war entry for a specifiic war id
function get_guild_war($warid) {
	$warid = (int)$warid; // Sanitizing the parameter id
	$row = $GLOBALS['dbh']->query("SELECT * FROM `guild_wars` WHERE `war_id`=$warid ORDER BY `started`;")->fetch(PDO::FETCH_ASSOC);
	
	return (!empty($row))?$row:false;
}

// List all war entries
function get_guild_wars() {
	$array = $GLOBALS['dbh']->query("SELECT * FROM `guild_wars` ORDER BY `started` DESC LIMIT 0, 30")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:false;
}

// List kill activity in wars.
function get_war_kills($war_id) {
	$war_id = (int)$war_id;

	try{
		while($value = $GLOBALS['dbh']->query("SELECT `death_id`, `killer_id`, `victim_id`, `time`, `war_id` FROM `pvp_deaths` WHERE `war_id`=$war_id ORDER BY `time` DESC LIMIT 0, 30")->fetchAll(PDO::FETCH_ASSOC)){//$array = $result->fetch_all(MYSQLI_ASSOC);
			$array[] = $value;
			$arr5 = get_player_guild_data($array[1]);
			$arr6 = get_player_guild_data($array[2]);
			$array[5] = $arr5['guild_id'];
			$array[6] = $arr6['guild_id'];
		}
	} catch(PDOException $e) {
		die($e->getMessage());
	}
	return (!empty($array))?$array:false;
}

function get_death_data($did) {
	$did = (int)$did; // Sanitizing the parameter id
	$array = $GLOBALS['dbh']->query("SELECT `id`, `guild_id`, `enemy_id`, `status`, `begin`, `end` FROM `guild_wars` ORDER BY `begin` DESC LIMIT 0, 30")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:false;
}

// Gesior compatibility port TFS .3
function gesior_sql_death($warid) {
	$warid = (int)$warid; // Sanitizing the parameter id
	$query = $GLOBALS['dbh']->query('SELECT `pd`.`id`, `pd`.`date`, `gk`.`guild_id` AS `enemy`, `p`.`name`, `pd`.`level` FROM `guild_kills` gk LEFT JOIN `player_deaths` pd ON `gk`.`death_id` = `pd`.`id` LEFT JOIN `players` p ON `pd`.`player_id` = `p`.`id` WHERE `gk`.`war_id` = ' . $warid . ' AND `p`.`deleted` = 0 ORDER BY `pd`.`date` DESC');
	$array = $query->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:false;
}
function gesior_sql_killer($did) {
	$did = (int)$did; // Sanitizing the parameter id
	$query = $GLOBALS['dbh']->query('SELECT `p`.`name` AS `player_name`, `p`.`deleted` AS `player_exists`, `k`.`war` AS `is_war` FROM `killers` k LEFT JOIN `player_killers` pk ON `k`.`id` = `pk`.`kill_id` LEFT JOIN `players` p ON `p`.`id` = `pk`.`player_id` WHERE `k`.`death_id` = ' . $did . ' ORDER BY `k`.`final_hit` DESC, `k`.`id` ASC');
	$array = $query->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:false;
}
// end gesior
// END GUILD WAR
// ADMIN FUNCTIONS
function set_ingame_position($name, $acctype) {
	$acctype = (int)$acctype;
	$char_id = user_character_id($name);

	$GLOBALS['dbh']->exec("UPDATE `players` SET `group_id` = '$acctype' WHERE `id` =$char_id;");
}

// Set rule violation. 
// Return true if success, query error die if failed, and false if $config['website_char'] is not recognized.
function set_rule_violation($charname, $typeid, $actionid, $reasonid, $time, $comment) {
	$charid = user_character_id($charname);
	$typeid = (int)$typeid;
	$actionid = (int)$actionid;
	$reasonid = (int)$reasonid;
	$time = (int)($time + time());
	
	$data = user_character_data($charid, 'account_id', 'lastip');
	
	$accountid = $data['account_id'];
	$charip = $data['lastip'];
	
	
	// ...
	$bannedby = config('website_char');
	if (user_character_exist($bannedby)) {
		$bannedby = user_character_id($bannedby);
		try{
			$sth = $GLOBALS['dbh']->prepare("INSERT INTO `bans` (`type` ,`ip` ,`mask` ,`player` ,`account` ,`time` ,`reason_id` ,`action_id` ,`comment` ,`banned_by`) VALUES (:type, :ip, :mask, :player, :account, :time, :reasonid, :actionid, :comment, :bannedby);");
			$sth->bindValue(':type', $typeid, PDO::PARAM_INT);
			$sth->bindValue(':ip', $charip, PDO::PARAM_INT);
			$sth->bindValue(':mask', '4294967295', PDO::PARAM_INT);
			$sth->bindValue(':player', $charid, PDO::PARAM_INT);
			$sth->bindValue(':account', $accountid, PDO::PARAM_INT);
			$sth->bindValue(':time', $time, PDO::PARAM_INT);
			$sth->bindValue(':reasonid', $reasonid, PDO::PARAM_INT);
			$sth->bindValue(':actionid', $actionid, PDO::PARAM_INT);
			$sth->bindValue(':comment', $comment, PDO::PARAM_STR);
			$sth->bindValue(':bannedby', $bannedby, PDO::PARAM_INT);
			$sth->execute();
			return true;
		} catch(PDOException $e) {
			//return false;
			die($e->getMessage());
		}

	} else {
		return false;
	}
}

// -- END admin
// Fetch deathlist
function user_fetch_deathlist($char_id) {
	$char_id = (int)$char_id;
	$array = $GLOBALS['dbh']->query("SELECT * FROM `player_deaths` WHERE `player_id`='$char_id' order by `time` DESC LIMIT 0, 10")->fetchAll(PDO::FETCH_NUM);
	return (!empty($array))?$array:false;
}

// TFS .3 compatibility
function user_fetch_deathlist03($char_id) {
	$char_id = (int)$char_id;
	$array = $GLOBALS['dbh']->query("SELECT * FROM `player_deaths` WHERE `player_id`='$char_id' order by `date` DESC LIMIT 0, 10")->fetchAll(PDO::FETCH_NUM);
	return (!empty($array))?$array:false;
}
// same
function user_get_kid($did) {
	$did = (int)$did;
	$result = $GLOBALS['dbh']->query("SELECT `id` FROM `killers` WHERE `death_id`='$did';")->fetch(PDO::FETCH_NUM);
	return $result[0];
}
// same
function user_get_killer_id($kn) { //what about multiple killers?
	$kn = (int)$kn;
	$query = $GLOBALS['dbh']->query("SELECT `player_id` FROM `player_killers` WHERE `kill_id`='$kn';");
	while($killer = $query->fetch(PDO::FETCH_NUM)){
		$row = $killer[0]; //will only get the last result?
	}
	
	return (!empty($row))?$row:false;
}
// same
function user_get_killer_m_name($mn) {
	$mn = (int)$mn;
	$result = $GLOBALS['dbh']->query("SELECT `name` FROM `environment_killers` WHERE `kill_id`='$mn';")->fetch(PDO::FETCH_NUM);
	return $result[0];
}

function user_count_deathlist($char_id) {
	$char_id = (int)$char_id;
	$result = $GLOBALS['dbh']->query("SELECT COUNT(*) FROM `player_deaths` WHERE `player_id`='$char_id' order by `time` DESC LIMIT 0, 10")->fetchColumn();
	return $result;
}

// MY ACCOUNT RELATED \\
function user_update_comment($char_id, $comment) {
	try{
		$sth = $GLOBALS['dbh']->prepare("UPDATE `znote_players` SET `comment`=:comment WHERE `player_id`=:playerid");
		$sth->bindValue(':comment', $comment, PDO::PARAM_STR);
		$sth->bindValue(':playerid', $char_id, PDO::PARAM_INT);
		$sth->execute();
		return true;
	} catch(PDOException $e) {
		die($e->getMessage());
	}
}

function user_delete_character($char_id) {
	$char_id = (int)$char_id;
	$GLOBALS['dbh']->exec("DELETE FROM `players` WHERE `id`='$char_id';");
	$GLOBALS['dbh']->exec("DELETE FROM `player_skills` WHERE `player_id`='$char_id';");
	$GLOBALS['dbh']->exec("DELETE FROM `znote_players` WHERE `player_id`='$char_id';");
}

// Parameter: accounts.id returns: An array containing detailed information of every character on the account.
// Array: [0] = name, [1] = experience, [2] = vocation, [3] = town_id, [4] = lastlogin, [5] = online
function user_character_list($account_id) {
	//$count = user_character_list_count($account_id);
	$query = $GLOBALS['dbh']->query("SELECT `name`, `experience`, `vocation`, `town_id`, `lastlogin`, `online` FROM `players` WHERE `account_id`='$account_id' ORDER BY `experience` DESC LIMIT 0, 8");
	$i=0;
	while($row = $query->fetch(PDO::FETCH_ASSOC)){
		$array[$i] = $row;
		$array[$i]['level'] = experience_to_level($row['experience']); // Change vocation id to vocation name
		$array[$i]['vocation'] = vocation_id_to_name($row['vocation']); // Change vocation id to vocation name
		$array[$i]['town'] = town_id_to_name($row['town_id']); // Change town id to town name
		// Make lastlogin human read-able. 
		if ($array[$i]['lastlogin'] != 0) {
			$array[$i]['lastlogin'] = date(config('date'),$row['lastlogin']);
		} else {
			$array[$i]['lastlogin'] = 'Never.';
		}
		
		$array[$i++]['online'] = online_id_to_name($row['online']); // 0 to "offline", 1 to "ONLINE". 


	}
	if (isset($array)) {return $array; } else {return false;}
}

function user_character_list_player_id($account_id) {
	//$count = user_character_list_count($account_id);
	$account_id = (int)$account_id;
	$query = $GLOBALS['dbh']->query("SELECT `id` FROM `players` WHERE `account_id`='$account_id' ORDER BY `experience` DESC LIMIT 0, 30");
	while($row = $query->fetch(PDO::FETCH_NUM)){
		$array[] = $row[0];
	}
	return (!empty($array))?$array:false;
}

// Parameter: accounts.id returns: number of characters on the account.
function user_character_list_count($account_id) {
	$account_id = (int)$account_id;
	$result = $GLOBALS['dbh']->query("SELECT COUNT(*) FROM players WHERE players.account_id=$account_id;")->fetchColumn();
	return $result;
}

// Only count visible characters! //TODO - bit tricky with new DB structure // Easy with left join ("SELECT COUNT(*) FROM players LEFT JOIN znote_players on znote_players.player_id=players.id WHERE players.account_id=$account_id AND znote_players.hide_char = 0;", should work, but all players have to be a field on znote_players, so if there is handmade chars, it will not count them i think), but shouldnt be used in the function above, as its on the account page where even the not visible chars shoud be seem and counted
/*
function user_character_list_count($account_id) {
	$account_id = sanitize($account_id);
	return mysql_result(mysql_query("SELECT COUNT('id') FROM `players` WHERE `account_id`='$account_id'"), 0);
}
*/
// END MY ACCOUNT RELATED

// HIGHSCORE FUNCTIONS \\(I think I will move this to an own file later)
function highscore_getAll() {
	$result = array();
	for ($i = 0; $i <= 6; $i++) {
		$result[$i] = highscore_skills($i);
	}
	$result[7] = highscore_experience();
	$result[8] = highscore_maglevel();

	return $result;
}

function highscore_skills($skillid) {
	$skilltries = skillid_to_column_name($skillid = (int)$skillid);
	$skill = explode ('_', $skilltries);
	$skill = $skill[0];
	//$count = highscore_count($skillid);
	try{
		$query = $GLOBALS['dbh']->query("SELECT `player_id`, `$skill`, `$skilltries` FROM `player_skills` ORDER BY `$skilltries` DESC LIMIT 0, 30");
		$stmt = $GLOBALS['dbh']->prepare("SELECT `vocation`, `name` FROM `players` WHERE `id`= :id;");
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);
		$i=0;
		while($row = $query->fetch(PDO::FETCH_NUM)){
			$array[$i] = $row;
			$id = $array[$i][0];
			$stmt->execute();
			$stmt->bindColumn('vocation', $vocation);
			$stmt->bindColumn('name', $name);
			if($values = $stmt->fetch(PDO::FETCH_BOUND)){ 
				$array[$i][2] = $vocation;
				$array[$i][0] = $name;
			} 
			/*
			not using for now,can be removed if we deiced not to drop skill values
			if ($skillid == 6)
				$array[$i][1] = tries_to_fishing($array[$i][1], $array[$i][2], $skilltries);
			else
				$array[$i][1] = tries_to_skill($array[$i][1], $array[$i][2], $skilltries);
			*/
			if ($skillid == 6 || $skillid == 5) { // If skillid is shielding or fishing, lets display vocation name instead of id.
				$vocs = config('vocations');
				$array[$i][2] = $vocs[$array[$i][2]];
			}
			$i++;
		}
	} catch (PDOException $e) {
		die($e->getMessage());
	}
	return (!empty($array))?$array:false;
}

function highscore_experience() {
	//$count = highscore_experience_count();
	$query = $GLOBALS['dbh']->query("SELECT `name`, `experience`, `vocation` FROM `players` WHERE `experience`>500 ORDER BY `experience` DESC LIMIT 0, 30");
	$i=0;
	while($row = $query->fetch(PDO::FETCH_NUM)){
		$array[$i] = $row;
		$vocs = config('vocations');
		$array[$i][2] = $vocs[$array[$i][2]];
		$array[$i][3] = experience_to_level($array[$i][1]);
		$i++;
	}
	return(!empty($array))?$array:false;
}

function highscore_maglevel() {
	//$count = highscore_experience_count(); // Dosn't matter if I count exp, maglvl is on same table.
	$query = $GLOBALS['dbh']->query("SELECT `name`, `manaspent`, `vocation` FROM `players` WHERE `experience`>500 ORDER BY `manaspent` DESC LIMIT 0, 30");
	$i=0;
	while($row = $query->fetch(PDO::FETCH_NUM)){
		$array[$i] = $row;
		$array[$i][1] = manaspent_to_ml($array[$i][1], $array[$i][2]);
		unset($array[$i++][2]); 
	}
	return (!empty($array))?$array:false;
}

function highscore_count($skillid) {
	$count = $GLOBALS['dbh']->query("SELECT COUNT(*) FROM `player_skills` LIMIT 0, 30")->fetchColumn();
	return $count;
}

function highscore_experience_count() {
	$count = $GLOBALS['dbh']->query("SELECT COUNT(*) FROM `players` WHERE `experience`>'500' LIMIT 0, 30")->fetchColumn();
	return $count;
}
// END HIGHSCORE FUNCTIONS


// NEWS FUNCTIONS
function get_news_list(){
	$array = $GLOBALS['dbh']->query("SELECT * FROM `znote_news` ORDER BY `created` DESC LIMIT 0, 30")->fetchAll(PDO::FETCH_ASSOC);
	return (!empty($array))?$array:array();
}

function get_news_by_id($edit){
	$edit = (int)$edit;
	$data = $GLOBALS['dbh']->query("SELECT * FROM `znote_news` WHERE `id` = $edit;")->fetch(PDO::FETCH_ASSOC);
	return $data;
}

function add_news($title, $body, $created){
	$sth = $GLOBALS['dbh']->prepare("INSERT INTO `znote_news` (title,text,created) VALUES (:title,:text,:created)");
	$sth->bindValue(':title', $title, PDO::PARAM_STR);
	$sth->bindValue(':text', $body, PDO::PARAM_STR);
	$sth->bindValue(':created', $created, PDO::PARAM_INT);
	$sth->execute();
	return true;
}

function edit_news($title, $body, $nid){
	$nid = (int)$nid;
	$sth = $GLOBALS['dbh']->prepare("UPDATE znote_news SET title=:title, text=:text WHERE id =:id");
	$sth->bindValue(':title', $title, PDO::PARAM_STR);
	$sth->bindValue(':text', $body, PDO::PARAM_STR);
	$sth->bindValue(':id', $nid, PDO::PARAM_INT);
}

function delete_news($nid){
	$nid = (int)$nid;
	$GLOBALS['dbh']->exec("DELETE FROM `znote_news` WHERE `id` = $nid");
}
//END NEWS FUNCTIONS

//START HOUSE FUNCTIONS
function get_house_list($order, $town, $state){
	$town = (int) $town;
	$state = (int) $state;
	
	$query = $GLOBALS['dbh']->query("SELECT * FROM `houses` WHERE `town`='$town' AND `for_sale`='$state'  ORDER BY `$order`");
	$i=0;
	while($row = $query->fetch(PDO::FETCH_ASSOC)){
		$array[$i] = $row;		
		$owner = user_character_data($row['owner'], 'name');
		if($owner)
			$array[$i]['owner'] = $owner['name'];
		else
			$array[$i]['owner'] = 'Nobody';
		$array[$i]['name'] = $row['name']; // Change vocation id to vocation name
		$array[$i]['size'] = $row['size']; // Change vocation id to vocation name
		$array[$i++]['rent'] = $row['rent']; // Change town id to town name
	}
	if (isset($array)) {return $array; } else {return false;}
}
//END HOUSE FUINCTIONS

function user_recover($mode, $edom, $email, $character, $ip) {
/* -- Lost account function - user_recovery --

	$mode = username/password recovery definition
	$edom = The remembered value. (if mode is username, edom is players password and vice versa)
	$email = email address
	$character = character name
	$ip = character IP
*/
	// Structure verify array data correctly
	if ($mode === 'username') {
		$verify_data = array(
			'password' => sha1($edom),
			'email' => $email
		);
	} else {
		$verify_data = array(
			'name' => $edom,
			'email' => $email
		);
	}
	// Determine if the submitted information is correct and herit from same account
	if (user_account_fields_verify_value($verify_data)) {
		
		// Structure account id fetch method correctly
		if ($mode == 'username') {
			$account_id = user_account_id_from_password($verify_data['password']);
		} else {
			$account_id = user_id($verify_data['name']);
		}
		// get account id from character name
		$player_account_id = user_character_account_id($character);
		
		//Verify that players.account_id matches account.id
		if ($player_account_id == $account_id) {
			// verify IP match (IP = accounts.email_new) \\
			// Fetch IP data
			$ip_data = user_znote_account_data($account_id, 'ip');
			if ($ip == $ip_data['ip']) {
				// IP Match, time to stop verifying SHIT and get on
				// With giving the visitor his goddamn username/password!
				if ($mode == 'username') {
					$name_data = user_data($account_id, 'name');
					echo '<br><p>Your username is:</p> <h3>'. $name_data['name'] .'</h3>';
				} else {
					$newpass = substr(sha1(rand(1000000, 99999999)), 8);
					echo '<br><p>Your new password is:</p> <h3>'. $newpass .'</h3><p>Remember to login and change it!</p>';
					user_change_password($account_id, $newpass);
				}
				// END?! no, but almost. :)
			} else { echo'IP does not match.'; }
		} else { echo'Account data does not match.'; }
	} else { echo'Account data does not match.'; }
}

function user_account_id_from_password($password) {
	$sth = $GLOBALS['dbh']->prepare("SELECT `id` FROM `accounts` WHERE `password`=?;");
	$sth->execute(array($password));
	if($result = $sth->fetch(PDO::FETCH_NUM))
		return $result[0];
}

function user_account_add_premdays($accid, $days) {
	$accid = (int)$accid;
	$days = (int)$days;
	$result = $GLOBALS['dbh']->query("SELECT `premdays` FROM `accounts` WHERE `id`='$accid';")->fetch(PDO::FETCH_NUM);
	$tmp = $result[0];
	$tmp += $days;
	$GLOBALS['dbh']->exec("UPDATE `accounts` SET `premdays`='$tmp' WHERE `id`='$accid'");
}

// Name = char name. Changes from male to female & vice versa.
function user_character_change_gender($name) {
	$user_id = user_character_id($name);
	$result = $GLOBALS['dbh']->query("SELECT `sex` FROM `players` WHERE `id`='$user_id';")->fetch(PDO::FETCH_NUM);
	$gender = $result[0];
	$new_gender = ($gender==1)?0:1;
	$GLOBALS['dbh']->exec("UPDATE `players` SET `sex`='$new_gender' WHERE `id`='$user_id'");
}

function user_character_account_id($character) {
	$sth = $GLOBALS['dbh']->prepare("SELECT `account_id` FROM `players` WHERE `name`=?;");
	$sth->execute(array($character));
	if($result = $sth->fetch(PDO::FETCH_NUM))
		return $result[0];
}

function user_account_fields_verify_value($verify_data) {
	$verify = array();
	$first = true;
	$sql = "SELECT COUNT('*') FROM `accounts` WHERE ";
	foreach (array_keys($verify_data) as $field) {
		if($first == false)
			$sql .= ' AND ';
		$sql .= '`'.$field.'` = :'. $field;
		$first = false;
	}
	$sql .= ' ;';
	$sth = $GLOBALS['dbh']->prepare($sql);
	$sth->execute($verify_data);
	$result = $sth->fetchColumn();
	return ($result==1) ? true : false;
}

function user_update_account($update_data, $type = false) {
	if($type == 'znote'){
		$database = 'znote_accounts';
		$compare = 'account_id';
	} else {
		$database = 'accounts';
		$compare = 'id';
	}
	$sql = "UPDATE `$database` SET ";
	$comma = false;
	foreach ($update_data as $field=>$data) {
		if($comma == true)
			$sql .= ', ';
		$sql .= "`$field` = :$field";
		$comma = true;
	}
	$user_id = (int)$_SESSION['user_id'];
	$sql .= " WHERE `$compare`=$user_id;";
	$sth = $GLOBALS['dbh']->prepare($sql);
	$sth->execute($update_data);
}
function user_change_password($user_id, $password) {
	$user_id = (int) $user_id;
	$salt = user_data($user_id, 'salt');
	$password = sha1($salt['salt'].$password);
	
	$GLOBALS['dbh']->exec("UPDATE `accounts` SET `password`='$password' WHERE `id`=$user_id");
}

// Parameter: players.id, value[0 or 1]. Togge hide.
function user_character_set_hide($char_id, $value) {
	$char_id = (int)$char_id;
	$value = (int)$value;
	
	$GLOBALS['dbh']->exec("UPDATE `znote_players` SET `hide_char`='$value' WHERE `player_id`=$char_id");
}

function user_create_account($register_data) {
	//array_walk($register_data, 'array_sanitize');
	
	$register_data['salt'] = generate_recovery_key(18);
	$register_data['password'] = sha1($register_data['salt'].$register_data['password']);
	
	$ip = $register_data['ip'];
	$created = $register_data['created'];
	$email = $register_data['email'];
	
	unset($register_data['ip']);
	unset($register_data['created']);
	unset($register_data['email']);
	
	$sql = "INSERT INTO `accounts` (`". implode('`, `', array_keys($register_data)) ."`) VALUES (:". implode(', :', array_keys($register_data)) .")";
	//$sql .= '(`'. implode('`, `', array_keys($register_data)) .'`) VALUES ('. implode(', :', array_keys($register_data)) .')';
	//$data = '\''. implode('\', \'', $register_data) .'\'';
	try{
		$sth = $GLOBALS['dbh']->prepare($sql);
		$sth->execute($register_data);
	} catch (PDOException $e) {
		die($e->getMessage());
	}
	$account_id = user_id($register_data['name']);
	try{
		$sth = $GLOBALS['dbh']->prepare("INSERT INTO `znote_accounts` (`account_id`, `email`, `ip`, `created`) VALUES (?, ?, ?, ?)");
		$sth->execute(array($account_id, $email, $ip, $created));
	} catch (PDOException $e) {
		die($e->getMessage());
	}
	
	//TO-DO: mail server and verification.
	// http://www.web-development-blog.com/archives/send-e-mail-messages-via-smtp-with-phpmailer-and-gmail/
}

// Returns $gid of a guild leader($cid).
function user_character_world_id($cid) {
	$cid = (int)$cid;
	$result = $GLOBALS['dbh']->query("SELECT `world_id` FROM `players` WHERE `id`='$cid';")->fetch(PDO::FETCH_NUM);
	return $result[0];
}

// CREATE CHARACTER
function user_create_character($character_data) {
	//array_walk($character_data, 'array_sanitize');
	$cnf = fullConfig();
	
	if ($character_data['sex'] == 1) {
		$outfit_type = $cnf['maleOutfitId'];
	} else {
		$outfit_type = $cnf['femaleOutfitId'];
	}
	
	$import_data = array(
		'name' => $character_data['name'],
		'world_id' => $character_data['world_id'],
		'group_id' => 1,
		'account_id' => $character_data['account_id'],
		'vocation' => $character_data['vocation'],
		'health' => $cnf['health'],
		'experience' => $cnf['exp'],
		'lookbody' => 0, /* STARTER OUTFITS */
		'lookfeet' => 0,
		'lookhead' => 0,
		'looklegs' => 0,
		'looktype' => $outfit_type,
		'lookaddons' => 0,
		'lookmount' => 0,
		'mana' => $cnf['mana'],
		'manaspent' => 0,
		'town_id' => $character_data['town_id'],
		'posx' => 1000,
		'posy' => 1000,
		'posz' => 7,
		'sex' => $character_data['sex'],
		'lastlogin' => 0,
		'skull' => 0,
		'stamina' => 151200000,
		'marriage' => 0,
		'lastlogin' => 0,
		'online' => 0,
		'depot' => '',
		'inventory' => '',
	);
	
	// If you are no vocation (id 0), use these details instead:
	if ($character_data['vocation'] === '0') {
		$import_data['experience'] = $cnf['nvExp'];
		$import_data['health'] = $cnf['nvHealth'];
		$import_data['mana'] = $cnf['nvMana'];
		
		if ($cnf['nvForceTown'] == 1) {
			$import_data['town_id'] = $cnf['nvTown'];
		}
	}
	$sql = "INSERT INTO `players` (`". implode('`, `', array_keys($import_data)) ."`) VALUES (:". implode(', :', array_keys($import_data)) .")";
	try{
		//creating
		$players = $GLOBALS['dbh']->prepare($sql);
		$players->execute($import_data);
		$charid = $GLOBALS['dbh']->lastInsertId(); //$charid = user_character_id($import_data['name']);
	
		//creating skills entry
		$GLOBALS['dbh']->exec("INSERT INTO `player_skills`(`player_id`) VALUES ($charid);");
	
		//creating znote_players entry
		//$charid = user_character_id($import_data['name']);
		$GLOBALS['dbh']->exec("INSERT INTO `znote_players`(`player_id`, `hide_char`, `comment`) VALUES ('$charid', '0', '');");
	} catch (PDOException $e) {
		die($e->getMessage());
	}
}

function user_count_online() {
	$result = $GLOBALS['dbh']->query("SELECT COUNT(*) from `players` WHERE `online` = 1;")->fetchColumn();
	return $result;
}

function user_count_accounts() {
	$result = $GLOBALS['dbh']->query("SELECT COUNT(*) from `accounts`;")->fetchColumn();
	return $result;
}

function user_character_data($user_id) {
	$data = array();
	$user_id = (int)$user_id;
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 1)  {
		unset($func_get_args[0]);
		
		$fields = '`'. implode('`, `', $func_get_args) .'`';

		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `players` WHERE `id` = $user_id;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
}

// return query data from znote_players table
function user_znote_character_data($character_id) {
	$data = array();
	$charid = (int)$character_id;
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 1)  {
		unset($func_get_args[0]);
		
		$fields = '`'. implode('`, `', $func_get_args) .'`';
		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `znote_players` WHERE `player_id` = $charid;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
}

// return query data from znote table
function user_znote_data() {
	$data = array();
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 0)  {
		$fields = '`'. implode('`, `', $func_get_args) .'`';
		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `znote`;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
}

// return query data from znote_accounts table
function user_znote_account_data($account_id) {
	$data = array();
	$accid = (int)$account_id;
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 1)  {
		unset($func_get_args[0]);
		$fields = '`'. implode('`, `', $func_get_args) .'`';
		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `znote_accounts` WHERE `account_id` = $accid;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
	
}

// return query data from znote_visitors table
function user_znote_visitor_data($longip) {
	$data = array();
	$longip = (int)$longip;
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 1)  {
		unset($func_get_args[0]);
		$fields = '`'. implode('`, `', $func_get_args) .'`';
		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `znote_visitors` WHERE `ip` = $longip;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
}

// return query data from znote_visitors_details table
function user_znote_visitor_details_data($longip) {
	$data = array();
	$longip = (int)$longip;
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 1)  {
		unset($func_get_args[0]);
		$fields = '`'. implode('`, `', $func_get_args) .'`';
		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `znote_visitors_details` WHERE `ip` = $longip;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
}

function user_data($user_id) {
	$data = array();
	$user_id = (int)$user_id;
	
	$func_num_args = func_num_args();
	$func_get_args = func_get_args();
	
	if ($func_num_args > 1)  {
		unset($func_get_args[0]);
		
		$fields = '`'. implode('`, `', $func_get_args) .'`';
		$data = $GLOBALS['dbh']->query("SELECT $fields FROM `accounts` WHERE `id` = $user_id;")->fetch(PDO::FETCH_ASSOC);
	}
	return $data;
}

function user_exist($username) {
	$stmt = $GLOBALS['dbh']->prepare("SELECT COUNT(*) FROM `accounts` WHERE `name`=?;");
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn();
	return ($result >= 1) ? true : false;
}

function user_character_exist($username) {
	$stmt = $GLOBALS['dbh']->prepare("SELECT COUNT(*) FROM `players` WHERE `name`=?;");
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn();
	return ($result >= 1) ? true : false;
}

function user_email_exist($email) {
	$stmt = $GLOBALS['dbh']->prepare("SELECT COUNT(*) FROM `accounts` WHERE `email`=?;");
	$stmt->execute(array($email));
	$result = $stmt->fetchColumn();
	return ($result >= 1) ? true : false;
}

function user_id_from_email($email) {
	$stmt = $GLOBALS['dbh']->prepare("SELECT `id` FROM `accounts` WHERE `email`=?;");
	$stmt->execute(array($email));
	$account_id = $stmt->fetchColumn();
	return $account_id;
}

function user_password_exist($password) { // Why you need to know if there is someone with a specific password already? Should be deprecated?
	$password = sha1($password);
	$result = $GLOBALS['dbh']->query("SELECT COUNT(*) FROM `accounts` WHERE `password`='$password';")->fetchColumn();
	return ($result == 1) ? true : false;
}

function user_password_match($password, $account_id) {
	$password = sha1($password); //What about $salt?
	$account_id = (int)$account_id;
	$result = $GLOBALS['dbh']->query("SELECT COUNT(*) FROM `accounts` WHERE `password`='$password' AND `id`='$account_id';")->fetchColumn();
	return ($result == 1) ? true : false;
}

function user_id($username) {
	$stmt = $GLOBALS['dbh']->prepare("SELECT `id` FROM `accounts` WHERE `name`=?;");
	$stmt->execute(array($username));
	$result = $stmt->fetchColumn();
	return $result;
}

// TFS 0.3+ compatibility.
function user_login_id($username, $password) {
	if (user_exist($username)) {
		$user_id = user_id($username);
		$result = $GLOBALS['dbh']->query("SELECT `salt` FROM `accounts` WHERE `id`='$user_id';")->fetchColumn();
		$salt = $result;
		if ($salt)
			$password = sha1($salt.$password);
		else
			$password = sha1($password);
		try{
			$stmt = $GLOBALS['dbh']->prepare("SELECT `id` FROM `accounts` WHERE `name`=? AND `password`=?;");
			$stmt->execute(array($username, $password));
			$result2 = $stmt->fetchColumn();
		} catch (PDOException $e) {
			$result2 =  false;
		}
			

		return $result2;
	}
}

function user_character_id($charname) {
	try{
		$stmt = $GLOBALS['dbh']->prepare("SELECT `id` FROM `players` WHERE `name`=?;");
		$stmt->execute(array($charname));
		$result = $stmt->fetchColumn();
	} catch (PDOException $e) {
		$result =  false;
	}
	return $result;
}

function user_character_hide($username) {
	$userid = user_character_id($username);
	try{
		$result = $GLOBALS['dbh']->query("SELECT `hide_char` FROM `znote_players` WHERE `player_id`=$userid;")->fetchColumn();
	} catch (PDOException $e) {
		$result =  false;
	}
	return $result;
}

function user_login($username, $password) {
	$user_id = user_login_id($username, $password);

	$result = $GLOBALS['dbh']->query("SELECT `salt` FROM `accounts` WHERE `id`='$user_id';")->fetchColumn();
	$salt = $result;
	if ($salt)
		$password = sha1($salt.$password);
	else
		$password = sha1($password);
	try{
		$stmt = $GLOBALS['dbh']->prepare("SELECT COUNT(*) FROM accounts WHERE name=? AND password=?;");
		$stmt->execute(array($username,$password));
		$result2 = $stmt->fetchColumn();
	} catch (PDOException $e) {
		$result2 = false;
	}

	return ($result2 == 1) ? $user_id : false;
}

function user_logged_in() {
	if(isset($_SESSION['user_id']) and $_SESSION['user_id'] != 0){
		return true;
	} else {
		return false;
	}
}
?>