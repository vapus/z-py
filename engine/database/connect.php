<?php
$time = time();
if (!isset($version)) $version = '1.0_PUBLIC';
$install = "
<h2>Install:</h2>
<ol>
	<li>
		<p>
			Make sure you have imported pyOT database. (pyOT/data/sql/latest_sql.sql)
			<br>This AAC is designed for pyOT.
		</p>
	</li>
	<li>
		Import the below schema to a <b>pyOT database in phpmyadmin</b>:<br>
		<textarea cols=\"65\" rows=\"10\">
CREATE TABLE `znote` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `version` varchar(30) NOT NULL COMMENT 'Znote AAC version',
  `installed` int(10) NOT NULL,
  `cached` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `znote` (`version`, `installed`) VALUES
('$version', '$time');

CREATE TABLE `znote_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `ip` int(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `created` int(10) NOT NULL,
  `points` int(10) DEFAULT 0,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

INSERT INTO `znote_accounts` (`account_id`, `ip`, `created`) VALUES
('1', '0', '$time');

CREATE TABLE IF NOT EXISTS `znote_news` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(30) NOT NULL,
  `text` text NOT NULL,
  `created` int(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `znote_news` (`title`, `text`, `created`) VALUES
('Z-PY AAC under developement.', 'TODO: <ul><li>Displaying Guild Wars</li><li>Death List(may still change on pyot)</li><li>Ban system(not yet made)</li><li>buying houses (optional)</li><li>inicating guild wars(optional)</li><li>Edit downloads.php to add download links to etc tibia client and IP changer.</li><li>Edit serverinfo.php to write some server information.</li><li>Edit contact.php to write some contact details.</li></ul>', '$time');

CREATE TABLE `znote_paypal` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `txn_id` varchar(30) NOT NULL,
  `email` varchar(255) NOT NULL,
  `accid` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `znote_players` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `player_id` int(11) NOT NULL,
  `hide_char` tinyint(4) NOT NULL,
  `comment` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

INSERT INTO `znote_players` (`player_id`, `hide_char`, `comment`) VALUES
('2', '0', '. . .');

CREATE TABLE `znote_shop` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `itemid` int(11) DEFAULT NULL,
  `count` int(11) NOT NULL DEFAULT '1',
  `describtion` varchar(255) NOT NULL,
  `points` int(11) NOT NULL DEFAULT '10',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `znote_shop_logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `player_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `points` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `znote_shop_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `account_id` int(11) NOT NULL,
  `type` int(11) NOT NULL,
  `itemid` int(11) NOT NULL,
  `count` int(11) NOT NULL,
  `time` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `znote_visitors` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` int(11) NOT NULL,
  `value` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

CREATE TABLE `znote_visitors_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ip` int(11) NOT NULL,
  `time` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `account_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
</textarea>
	</li>
	<li>
		<p>
			Edit config.php with correct mysql connection details.
		</p>
	</li>
</ol>
";
try {
    $dbh = new PDO("mysql:host=".$config['sqlHost'].";dbname=".$config['sqlDatabase'], $config['sqlUser'], $config['sqlPassword']);
    $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING);// PDO::ERRMODE_WARNING => Development, PDO::ERRMODE_EXCEPTION =>Production
} catch(PDOException $e) {
    //echo $e->getMessage();
	die('<h1>Failed to connect to database.</h1>'. $install);
 }
?>